'use strict';

/**
 * Module dependencies.
 */
var mongoose = require('mongoose'),
	Timesheet = mongoose.model('Timesheet'),
	_ = require('lodash');

/**
 * Get the error message from error object
 */
var getErrorMessage = function(err) {
	var message = '';

	if (err.code) {
		switch (err.code) {
			case 11000:
			case 11001:
				message = 'Timesheet already exists';
				break;
			default:
				message = 'Something went wrong';
		}
	} else {
		for (var errName in err.errors) {
			if (err.errors[errName].message) message = err.errors[errName].message;
		}
	}

	return message;
};

/**
 * Create a Timesheet
 */
exports.create = function(req, res) {
	var timesheet = new Timesheet(req.body);
	timesheet.updated = Date();
	timesheet.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(timesheet);
		}
	});
};

/**
 * Show the current Timesheet
 */
exports.read = function(req, res) {
	res.jsonp(req.timesheet);
};

/**
 * Update a Timesheet
 */
exports.update = function(req, res) {
	var timesheet = req.timesheet ;
	console.log('exports update');
	timesheet.updated = Date();
	timesheet = _.extend(timesheet , req.body);

	timesheet.save(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(timesheet);
		}
	});
};

/**
 * Delete an Timesheet
 */
exports.delete = function(req, res) {
	var timesheet = req.timesheet ;

	timesheet.remove(function(err) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(timesheet);
		}
	});
};

/**
 * List of Timesheets
 */
exports.list = function(req, res) { Timesheet.find().sort('-created').populate('user', 'displayName').exec(function(err, timesheets) {
		if (err) {
			return res.send(400, {
				message: getErrorMessage(err)
			});
		} else {
			res.jsonp(timesheets);
		}
	});
};

/**
 * Timesheet middleware
 */
exports.timesheetByID = function(req, res, next, id) {
    Timesheet.findById(id).populate('project','client vendor user').exec(function(err, timesheet) {
		if (err) return next(err);
		if (! timesheet) return next(new Error('Failed to load Timesheet ' + id));
		req.timesheet = timesheet ;
		Timesheet.populate(timesheet, {path:'project.user', select: 'displayName', model:'User'},function(err, results){
			if(err){
				next();
				return;
			}
			Timesheet.populate(timesheet, {path:'project.vendor', select: 'name', model:'Vendor'},function(err, results){
				next();
			});
		});
	});
};

/**
 * Timesheet authorization middleware
 */
exports.hasAuthorization = function(req, res, next) {
    //TODO: Access Control
	if (false && req.timesheet.user.id !== req.user.id) {
		return res.send(403, 'User is not authorized');
	}
	next();
};
