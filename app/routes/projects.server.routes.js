'use strict';

module.exports = function(app) {
	var users = require('../controllers/users.server.controller');
	var projects = require('../controllers/projects.server.controller');
	var fileUpload = require('../modules/file.upload');
	var mongoose = require('mongoose');
	var Model = mongoose.model('Project');

	// Projects Routes
	app.route('/projects')
		.get(users.hasAuthorization(['admin']),projects.list)
		.post(users.hasAuthorization(['admin']), projects.create);

    app.route('/projects/getList')
        .get(users.hasAuthorization(['admin']),projects.getList);

    // project Upload Routes
    app.route('/projects/upload')
        .post(users.hasAuthorization(['admin']), fileUpload.files.array('file'), fileUpload.upload(Model));

	app.route('/projects/download/:modelId/*')
		.get(fileUpload.download(Model));

    app.route('/projects/deleteDoc')
        .post(users.hasAuthorization(['admin']),fileUpload.deleteDoc(Model));
    
	app.route('/projects/:projectId')
		.get(users.hasAuthorization(['admin']),projects.read)
		.put(users.hasAuthorization(['admin']), projects.update)
		.delete(users.hasAuthorization(['admin']), projects.delete);

	// Finish by binding the Project middleware
	app.param('projectId', projects.projectByID);
};