'use strict';
angular.module('core').directive('oiUpdatedTimesheets', function(){
	return{
		restrict: 'E',
		templateUrl: 'modules/users/views/oi-updatedTimesheets.html'
	};
});