'use strict';
/* globals _ */
// Users controller
angular.module('users').controller('UsersController', ['$scope', '$stateParams', '$location', '$http', '$upload', 'Authentication',
    'Users', 'Projects', 'UserUtils','TimesheetUtils', 'md5',
	function($scope, $stateParams, $location, $http, $upload, Authentication, Users, Projects, UserUtils, TimesheetUtils, md5) {
		$scope.authentication = Authentication;

		if($scope.authentication.user && _.indexOf($scope.authentication.user.roles, 'admin')!==-1){
			$scope.isAdmin = true;
		}
		// Create new User
		$scope.create = function() {

            if($scope.action==='Edit'){
                return $scope.update();
            }

			// Create new User object
			var user = new Users (this.user);

			// Redirect after save
			user.$save(function(response) {
				$location.path('users/' + response._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Remove existing User
		$scope.remove = function( user ) {
			if(prompt('Please enter word "DELETE" to delete this record').toLowerCase()==='delete'){
				if (user) {
					user.$remove();
					for (var i in $scope.users ) {
						if ($scope.users [i] === user ) {
							$scope.users.splice(i, 1);
						}
					}
				} else {
					$scope.user.$remove(function() {
						$location.path('users');
					});
				}
			}
		};

		// Update existing User
		$scope.update = function() {
			var user = $scope.user ;
			user.$update(function() {
				$location.path('users/' + user._id);
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

        //TODO: this should be part of timesheet model
        $scope.getTotal = function(daily){
            var result = 0;
            if(daily){
                for(var i=0;i<daily.length;i++){
                    var tmp = parseFloat(daily[i].hours);
                    if(!tmp){
                        tmp = 0;
                    }
                    result += tmp;
                }
            }
            return result;
        };

		// Find a list of Users
		$scope.find = function() {
			$scope.users = Users.query();
		};

        $scope.generateTimesheets = function(user){
            $scope.user = user;
            var t = TimesheetUtils.generate(user, user.projects, user.timesheets);
        };

        // Get all the data related to USER
        $scope.getAllData = function(){
            UserUtils.getAll($stateParams.userId).then(function(data){
                $scope.generateTimesheets(data);
                console.log('in get all data: ');
                	console.log(data);
            }, function(response){
                console.log('Error in getting user data');
            });
        };

        //Navigate to timesheet
        $scope.gotoTimesheet = function(user, project,timesheet){
            timesheet.md5 = md5.createHash(user._id+project._id+timesheet.start+timesheet.end);
            TimesheetUtils.timesheets[timesheet.md5] = {user:user, project: project, timesheet: timesheet};
            $location.path('timesheets/get/' + timesheet.md5);
        };

		// Find existing User
		$scope.findOne = function() {
            $scope.action = 'New';
            if($stateParams.userId){
                $scope.action = 'Edit';
                $scope.user = Users.get({
                    userId: $stateParams.userId
                },function(data){
					if(_.indexOf(data.roles, 'admin')!==-1){
						data.isAdmin = true;
					}
				});
            }
 		};

        $scope.addOne = function(key){
            if(!$scope.projects){
                $scope.projects = Projects.getList();
            }
            if(!$scope[key]){
                $scope[key] = [];
            }
            $scope[key].push({});
        };
	}
]);