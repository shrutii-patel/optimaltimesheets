'use strict';

// Users service used for communicating with the users REST endpoint
angular.module('users').factory('Users', ['$resource',
	function($resource) {
		return $resource('users/:userId', {
			userId:'@_id'
		}, {
			update: {
				method: 'PUT'
			},
			getList:{
				url: 'users/getList',
				method: 'GET',
				isArray:true
			}
		});
	}
])
.factory('UserUtils', ['$http', '$q',function($http, $q){
        return {
            getAll:function(userId){
                var xhr = $http.get;
                if(userId){
                    xhr = $http.post;
                }
                var deferred = $q.defer();
                xhr('/users/data', {user:userId}).success(function(response) {
                    deferred.resolve(response);
                }).error(function(response) {
                    deferred.reject(response);
                });
                return deferred.promise;
            }
        };
    }
]);