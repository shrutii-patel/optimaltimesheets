'use strict';

// Expenses controller
angular.module('expenses').controller('ExpensesController', ['$scope', '$stateParams', '$location', 'Authentication', 'Expenses', 'Users', '$window',
	function($scope, $stateParams, $location, Authentication, Expenses, Users, $window ) {
		$scope.authentication = Authentication;
		$scope.users = Users.getList();
		// Create new Expense
		$scope.save = function(status) {

			var expense = $scope.expense ;
			expense.status = status;

			if(!expense.endDate){
				expense.endDate = expense.startDate;
			}

			if($scope.action==='Edit'){
				return $scope.update();
			}
			// Create new Expense object
			expense = new Expenses ($scope.expense);

			// Redirect after save
			expense.$save(function(response) {
				$location.path('expenses');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});

		};

		// Remove existing Expense
		$scope.remove = function( expense ) {
			if ( expense ) { expense.$remove();

				for (var i in $scope.expenses ) {
					if ($scope.expenses [i] === expense ) {
						$scope.expenses.splice(i, 1);
					}
				}
			} else {
				$scope.expense.$remove(function() {
					$location.path('expenses');
				});
			}
		};

		// Update existing Expense
		$scope.update = function() {
			var expense = $scope.expense ;

			expense.$update(function() {
				$location.path('expenses');
			}, function(errorResponse) {
				$scope.error = errorResponse.data.message;
			});
		};

		// Find a list of Expenses
		$scope.find = function() {
			$scope.expenses = Expenses.query();
		};

		// Find existing Expense
		$scope.findOne = function() {
			$scope.action = 'New';
			if($stateParams.expenseId){
				$scope.action = 'Edit';
				$scope.expense = Expenses.get({
					expenseId: $stateParams.expenseId
				});
			}
		};
		$scope.goBack = function(){
			$window.history.back();
		};
	}
]);